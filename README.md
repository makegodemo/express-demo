# CI/CD Setup 
## GitLab project variables:
The following project variables are expected to be populated
- KUBECONFIG (file, masked) cluster/credential info (i.e. contents for ~/.kube/config)
- CI_REGISTRY_USER and CI_REGISTRY_PASSWORD (variable, masked) with the username/password to login to the registry
- CI_REGISTRY (variable) set to the Docker registry (i.e. host:port) 
